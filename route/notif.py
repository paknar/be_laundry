from util.util_waktu import dateTimeNow
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime
import math

from function.notif import GetNotifOr404
from model.default import NotifData, NotifBase, NotifOnDB, NotifPage, JwtToken
from model.util import SearchRequest, FieldObjectIdRequest
from util.util import CreateArrayCriteria, ValidateObjectId, IsiDefault, CreateCriteria
from .auth import get_current_user

router_notif = APIRouter()
dbase = MGDB.tbl_notif


@router_notif.post("/get_admin_notifs", response_model=dict)
async def get_admin_notifs(
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = -1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(
        get_current_user, scopes=["superadmin,admin", "*"]
    ),
):
    """
    Ini get notif based on company id (apabila current user adalah admin), dan get notif based on user id (apabila current user bukan admin).

    Apabila user adalah superadmin, get SEMUA data notifikasi, nggak peduli company id maupun user id-nya.
    """
    skip = page * size
    criteria = CreateArrayCriteria("notif", search)
    sort = "notif." + sort
    paging = {
        "$facet": {"data": [{"$sort": {sort: dir}}, {"$skip": skip}, {"$limit": size}]}
    }
    count = {"$count": "total"}
    #cek roles
    if "superadmin" in current_user.roles:
        print("superadmin")
        hakAkses = {}
    elif "admin" in current_user.roles:
        hakAkses = {"companyId": ObjectId(current_user.companyId)}
    else:
        hakAkses = {"userId": ObjectId(current_user.userId)}
    # print(criteria)
    # print([{'$unwind':{'path':'$notif'}},{'$project':{'_id':0,'notif':1}},{'$match':criteria},paging])
    print(
        [
            {"$match": hakAkses},
            {"$unwind": {"path": "$notif"}},
            {"$project": {"_id": 0, "notif": 1}},
            {"$match": criteria},
            paging,
        ]
    )
    datas_cursor = dbase.aggregate(
        [
            {"$match": hakAkses},
            {"$unwind": {"path": "$notif"}},
            {"$project": {"_id": 0, "notif": 1}},
            {"$match": criteria},
            {"$replaceRoot": {"newRoot": "$notif"}},
            paging,
        ]
    )
    
    datas = await datas_cursor.to_list(length=size)
    jml = dbase.aggregate(
        [
            {"$match": hakAkses},
            {"$unwind": {"path": "$notif"}},
            {"$match": criteria},
            count,
        ]
    )
    jmls = await jml.to_list(1000)
    total = 0
    if len(jmls) > 0:
        total = jmls[0]["total"]

    totalPages = math.ceil(total / size)
    reply = NotifPage(
        # content=datas[0]["data"],
        content=datas[0]["data"],
        size=size,
        page=page,
        totalElements=total,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_notif.get("/get_notif_user_id/{userId}", response_model=List[NotifData])
async def get_notif_user_id(
    userId: str,
    current_user: JwtToken = Security(
        get_current_user, scopes=["superadmin,admin", "*"]
    ),
):
    """
    Get all notifs by user id
    """
    notif = await dbase.find_one({"userId": ObjectId(userId)})
    if notif:
        return notif["notif"]
    else:
        raise HTTPException(status_code=404, detail="NotifData not found")


@router_notif.post("/get_my_notifs", response_model=dict)
async def get_my_notifs(
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = -1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    """
    Ini get notif based on current user id.
    """
    skip = page * size
    criteria = CreateArrayCriteria("notif", search)
    sort = "notif." + sort
    paging = {
        "$facet": {"data": [{"$sort": {sort: dir}}, {"$skip": skip}, {"$limit": size}]}
    }
    count = {"$count": "total"}
    hakAkses = {"userId": ObjectId(current_user.userId)}
    # print(criteria)
    # print([{'$unwind':{'path':'$notif'}},{'$project':{'_id':0,'notif':1}},{'$match':criteria},paging])
    print(
        [
            {"$match": hakAkses},
            {"$unwind": {"path": "$notif"}},
            {"$project": {"_id": 0, "notif": 1}},
            {"$match": criteria},
            paging,
        ]
    )
    datas_cursor = dbase.aggregate(
        [
            {"$match": hakAkses},
            {"$unwind": {"path": "$notif"}},
            {"$project": {"_id": 0, "notif": 1}},
            {"$match": criteria},
            {"$replaceRoot": {"newRoot": "$notif"}},
            paging,
        ]
    )
    datas = await datas_cursor.to_list(length=size)

    jml = dbase.aggregate(
        [
            {"$match": hakAkses},
            {"$unwind": {"path": "$notif"}},
            {"$match": criteria},
            count,
        ]
    )
    jmls = await jml.to_list(1000)
    total = 0
    if len(jmls) > 0:
        total = jmls[0]["total"]

    totalPages = math.ceil(total / size)
    reply = NotifPage(
        content=datas[0]["data"],
        size=size,
        page=page,
        totalElements=total,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_notif.get("/get_my_notif_id/{id}", response_model=NotifData)
async def get_my_notif_id(
    id: ObjectId = Depends(ValidateObjectId),
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    """
    Get notif berdasarkan id, dan user id notif mesti sama dengan user id current user.
    Dengan kata lain, ini seperti get my notifs, tapi id-nya bisa dispesifikasikan.
    """
    notif = await dbase.find_one(
        {"userId": ObjectId(current_user.userId), "notif.id": id}, {"notif.$": 1}
    )
    if notif:
        return notif["notif"][0]
    else:
        raise HTTPException(status_code=404, detail="NotifData not found")


@router_notif.delete("/delete_my_notif_id/{id}", response_model=dict)
async def delete_my_notif_id(
    id: ObjectId = Depends(ValidateObjectId),
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    """
    Delete my notif, berdasarkan id notifikasi. user id current user = user id notif (i.e. user nggak bisa mendelete notif yang bukan miliknya)
    """
    print({"userId": ObjectId(current_user.userId), "notif.id": id})
    notif_op = await dbase.update_one(
        {"userId": ObjectId(current_user.userId), "notif.id": id},
        {"$pull": {"notif": {"id": id}}},
    )
    if notif_op.modified_count:
        return {"status": f"deleted count: {notif_op.modified_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")
