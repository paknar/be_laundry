from util.util_excel import getNumber, getString
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security, File, UploadFile, Request, Form
from typing import List
from datetime import datetime
import logging
import math
import pandas as pd

from model.util import SearchRequest, FieldObjectIdRequest, DefaultData
from model.default import JwtToken
from util.util import ListToUp, ValidateObjectId, IsiDefault, CreateCriteria
from route.auth import get_current_user
from util.util_waktu import dateTimeNow
from model.laundry import laundryBase, laundryOnDB, laundryPage
import base64
import shutil
import os

router_laundry = APIRouter()
dbase = MGDB.tbl_laundry

async def GetlaundryOr404(id_: str):
    _id = ValidateObjectId(id_)
    laundry = await dbase.find_one({"_id": _id})
    if laundry:
        return laundry
    else:
        raise HTTPException(status_code=404, detail="Data Not Found")

# =================================================================================


@router_laundry.post("/laundry", response_model = laundryOnDB)
async def add_laundry(
    data_in: laundryBase,
    request : Request,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
) :
    laundry = IsiDefault(data_in, current_user)
    laundry.name = laundry.name.upper()
    laundry.input_time = str(dateTimeNow())
    laundry.status = "Baru"
    print(laundry.dict())

    laundry_op = await dbase.insert_one(laundry.dict())
    if laundry_op.inserted_id:
        laundry = await GetlaundryOr404(laundry_op.inserted_id)
        return laundry

@router_laundry.post("/get_laundry", response_model = dict())
async def get_all_laundry(
    size: int = 100000,
    page: int = 0,
    sort: str = "createTime",
    dir: int = 1,
    search: SearchRequest = None,
    request: Request = None,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
) :
    skip = page * size
    search.defaultObjectId.append(
    FieldObjectIdRequest(field="creatorUserId", key=ObjectId(current_user.userId))
    )
    petugas = []
    criteria = CreateCriteria(search)
    if 'admin' in current_user.roles or 'superadmin' in current_user.roles : 
        datas_cursor = dbase.find({"isDelete": False})
        petugas_cursor =  MGDB.user_petugas.find({"isDelete": False})
        petugas = await petugas_cursor.to_list(length=size)
        # print("oi")
    else :
        datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)

    
    # print(petugas)

    reply = laundryPage(
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
        data=petugas
    )
    # print(datas)
    return reply


@router_laundry.get("/laundry/{id_}", response_model=laundryOnDB)
async def get_laundry_by_id(
    id_: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    # print(id_)
    laundry = await dbase.find_one(
        {"_id": ObjectId(id_)}
    )
    if laundry:
        return laundry
    else:
        raise HTTPException(status_code=404, detail="laundry not found")
                                                                                                                                                                                                                                                                                                                                                      

@router_laundry.put("/laundry/{id_}", response_model=laundryOnDB)
async def update_laundry(
    id_: str,
    data_in: laundryBase,
    request : Request,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):

    laundry = IsiDefault(data_in, current_user, True)
    laundry.updateTime = dateTimeNow()

    laundry_op = await dbase.update_one(
        {"_id": ObjectId(id_), "creatorUserId": ObjectId(current_user.userId)},
        {"$set": laundry.dict(skip_defaults=True)},
    )
    # print(id_)
    if laundry_op.modified_count:
        return await GetlaundryOr404(id_)
    else:
        raise HTTPException(status_code=304)

@router_laundry.delete("/laundry/{id_}", dependencies=[Depends(GetlaundryOr404)], response_model=dict)
async def delete_laundry_by_id(
    id_: str, 
    request: Request,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    # token = await request.json()
    
    # current_user = await get_current_user(["*", "*"], token=token["token"])
    
    # laundry_op = await dbase.delete_one(
    #     {"_id": ObjectId(id_), "creatorUserId": ObjectId(current_user.userId)}
    # )

    laundry_op = await dbase.update_one(
        {"_id": ObjectId(id_)},
        {"$set": {"isDelete" : True}},
    )
    print(id_)
    if laundry_op.modified_count:
        return {"status": f"deleted count: {laundry_op.modified_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")

@router_laundry.get("/count_home", response_model=dict)
async def count_home_admin (
    request : Request,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    laundry_cursor = dbase.find({"isDelete": False, "creatorUserId" : current_user.userId})
    laundry = await laundry_cursor.to_list(length=100000)
    count_laundry = len(laundry)

    return {
        "count_laundry" : count_laundry
    }
    # print(count)