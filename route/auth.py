from datetime import timedelta
from typing import List

from sqlalchemy.sql.elements import Null
from model.support import AutomaticLoginEnum, LanguageEnum
from pydantic.utils import Obj
from function.notif import CreateNotif
from function.company import CreateCompany, GetCompByInitial, GetCompanyOr404
from bson.objectid import ObjectId
from fastapi import (
    APIRouter,
    Depends,
    HTTPException,
    status,
    Response,
    Security,
    BackgroundTasks,
)
from fastapi.security import (
    OAuth2PasswordBearer,
    OAuth2PasswordRequestForm,
    SecurityScopes,
)
import jwt

from config.config import MGDB, SECRET_KEY, ENVIRONMENT

from model.default import (
    CompanyBasic,
    IdentityData,
    JwtToken,
    NotifBase,
    NotifData,
    UserCredential,
    UserBasic,
    UserDataBase,
    UserOnDB,
    UserTipeEnum,
)
from model.util import EmailData

from function.user import PWDCONTEXT, CreateUser, GetUserOr404

from util.util import (
    CheckList,
    ValidPhoneNumber,
    ValidateEmail,
    RandomNumber,
    RandomString,
    ValidPassword,
    CekValidObjectId,
)
from util.util_waktu import (
    convertStrDateToDate,
    dateNowStr,
    dateTimeNow,
    dateTimeNowStr,
)

ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 1000

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/api_laundry/auth/login")

router_auth = APIRouter()


def create_access_token(data: JwtToken, expires_delta: timedelta = None):
    if expires_delta:
        # if(expires_delta = -1)
        expire = dateTimeNow() + expires_delta
    else:
        expire = dateTimeNow() + timedelta(minutes=1000)
    data.exp = expire
    encoded_jwt = jwt.encode(data.dict(), SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


async def create_token_by_username_password(
    tblName: str, username: str, password: str, companyId: str
):
    criteria = []
    criteria.append({"isDelete": False})
    if len(companyId) > 8:
        criteria.append({"companyId": ObjectId(companyId)})

    if CekValidObjectId(username):
        print("login with objectid")
        criteria.append({"userId": ObjectId(username)})
    elif ValidPhoneNumber(username) == True:
        print("login with phone")
        criteria.append({"phone": username})
    elif username.count("@") > 0:
        print("login with email")
        criteria.append({"email": username.lower()})
    else:
        print("login with username")
        criteria.append({"username": username.upper()})

    if len(criteria) == 1:
        print("cek punya berapa company")
        users_cursor = MGDB[tblName].find({"$and": criteria})
        users = await users_cursor.to_list(length=100)
        if len(users) > 1:
            print("pilih company dulu trus login ulang")
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Login harus menyertakan Company ID",
                headers={"WWW-Authenticate": "Bearer"},
            )
    user = await MGDB[tblName].find_one({"$and": criteria})
    print("User: " + str(user))
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Username atau Password tidak sesuai",
            headers={"WWW-Authenticate": "Bearer"},
        )

    # salah password
    if not PWDCONTEXT.verify(password, user["credential"]["password"]):
        if user["credential"]["otp"] == password:
            MGDB[tblName].update_one(
                {"userId": ObjectId(user["userId"])},
                {"$set": {"credential.otp": RandomString(20)}},
            )
        elif user["credential"]["token"] == password:
            MGDB[tblName].update_one(
                {"userId": ObjectId(user["userId"])},
                {"$set": {"credential.token": RandomString(20)}},
            )
        else:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Incorrect username or password",
                headers={"WWW-Authenticate": "Bearer"},
            )

    # cek validationStatus dan isActive
    if not user["credential"]["isActive"]:
        print("user tidak aktif")
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="user tidak aktif",
            headers={"WWW-Authenticate": "Bearer"},
        )

    data_token = JwtToken()
    data_token.tblName = user["credential"]["tblName"]
    data_token.producer = "erp"
    data_token.userId = str(user["userId"])
    data_token.companyId = str(user["companyId"])
    data_token.isAdmin = user["credential"]["isAdmin"]
    data_token.roles = user["credential"]["role"]

    # language data token
    if "lang" in user:
        if user["lang"]:
            data_token.lang = user["lang"]

    return data_token


async def get_current_user(
    security_scopes: SecurityScopes, token: str = Depends(oauth2_scheme)
):
    # print(security_scopes)
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        # print(payload)
        if payload.get("userId") is None:
            raise credentials_exception
        token_data = JwtToken()
        token_data.tblName = payload.get("tblName")
        token_data.producer = payload.get("producer")
        token_data.userId = payload.get("userId")
        token_data.companyId = payload.get("companyId")
        token_data.isAdmin = payload.get("isAdmin")
        token_data.roles = payload.get("roles")
        token_data.exp = payload.get("exp")
        token_data.lang = payload.get("lang")

        # Apabila user adalah admin / superadmin, secara default bisa mengakses SEMUA endpoint
        if CheckList(token_data.roles, ["superadmin", "admin"]):
            print("User adalah admin/superadmin")
            return token_data

        if security_scopes.scopes[0] == "*":
            print("semua role bisa akses")
        else:
            role = security_scopes.scopes[0].split(",")
            if CheckList(token_data.roles, role) is False:
                raise HTTPException(status_code=401, detail="Role is not allowed")

        if security_scopes.scopes[1] == "*":
            print("semua solution bisa akses")
        else:
            solution = security_scopes.scopes[1].split(",")
            if CheckList(token_data.solutions, solution) is False:
                raise HTTPException(status_code=401, detail="Solution is not allowed")
    except jwt.PyJWTError:
        raise credentials_exception
    return token_data


@router_auth.get("/init/{key}", response_model=dict)
async def init_reset(key: str):
    if key == "reset-user" and ENVIRONMENT != "production":
        print("reset")
        allCol = await MGDB.list_collection_names()
        for x in allCol:
            print(x)
            await MGDB.drop_collection(x)

        # insert tbl company
        comp = CompanyBasic()
        comp.name = "data center"
        comp.initial = "DC"
        # comp.solution = ["admin", "crm_katalis","ticketing","marketplace"]
        comp.solution = [
            "admin",
            "petugas"
        ]
        comp.picName = "admin"
        comp.picEmail = "admin@katalis.info"
        comp_op = await CreateCompany(comp)

        # insert tbl user admin
        user = UserCredential()
        user.name = "admin"
        user.email = "admin@katalis.info"
        user.phone = "0812345678"
        user.username = "admin"
        user.companyId = comp_op.companyId
        identity = IdentityData()
        identity.dateOfBirth = dateNowStr()
        user.identity = identity
        user_op = await CreateUser(
            "user_admin", user, "pass", False, ["superadmin", "admin"]
        )

        await MGDB["user_admin"].update_one(
            {"userId": user_op.userId}, {"$set": {"username": "ADMIN"}}
        )
        return {"status": 200}
    else:
        return {"status": 401}


@router_auth.get("/get_token_self/{companyId}/{userTipe}", response_model=dict)
async def get_token(companyId: str, userTipe: UserTipeEnum):
    # create token
    data_token = JwtToken()
    data_token.tblName = userTipe
    data_token.userId = str(ObjectId())
    data_token.producer = "dummy"
    data_token.companyId = companyId

    access_token_expires = timedelta(minutes=30)
    access_token = create_access_token(data_token, expires_delta=access_token_expires)
    return {"access_token": access_token}


@router_auth.post("/login", response_model=dict)
async def login_for_access_token(
    response: Response, form_data: OAuth2PasswordRequestForm = Depends()
):
    # login dengan nohp atau username nama_table*username/email/nohp*companyId
    form_data.username = form_data.username.strip()
    arUsername = form_data.username.split("*")
    if len(arUsername) != 2:
        if len(arUsername) == 3:
            companyId = arUsername[2]
        else:
            raise HTTPException(status_code=400, detail="Format Login tidak sesuai")
    tblName = arUsername[0]
    username = arUsername[1]
    form_data.password = form_data.password.strip()
    companyId = ""

    # cari company id berdasarkan username atau ada di parameter login
    if username.count(".") == 1 and not username.count("@") == 1:
        arInitial = username.split(".")
        dCompany = await GetCompByInitial(arInitial[0].upper())
        companyId = str(dCompany["companyId"])

    data_token = await create_token_by_username_password(
        tblName, username, form_data.password, companyId
    )
    user = await MGDB[tblName].find_one({"userId": ObjectId(data_token.userId)})
    # print(data_token.companyId)
    dComp = await GetCompanyOr404(data_token.companyId)
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(data_token, expires_delta=access_token_expires)

    # btoken = "Bearer " + str(access_token.decode("utf-8"))
    btoken = "Bearer " + str(access_token)

    # tambah header
    response.headers["Authorization"] = btoken

    language = "id"
    if "lang" in user:
        if user["lang"]:
            language = user["lang"]

    dateOfBirth = None
    if user["identity"]:
        if user["identity"]["dateOfBirth"]:
            dateOfBirth = user["identity"]["dateOfBirth"]

    return {
        "access_token": access_token,
        "userId": data_token.userId,
        "companyId": data_token.companyId,
        "companyName": dComp["name"],
        "active": True,
        "isAdmin": data_token.isAdmin,
        "role": data_token.roles,
        "name": user["name"],
        "image": user["profileImage"],
        "email": user["email"],
        "phone": user["phone"],
        "isWa": user["isWa"],
        "dateOfBirth": dateOfBirth,
        "isFirstLogin": user["credential"]["isFirstLogin"],
        "lang": language,
    }


@router_auth.post(
    "/user_create_password_first_login_and_update_profile", response_model=dict
)
async def user_create_password_first_login(
    newPassword: str,
    email: str,
    phone: str,
    isWa: bool,
    dateOfBirth: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    tblName = current_user.tblName
    if len(email) > 5:
        if not ValidateEmail(email):
            raise HTTPException(status_code=400, detail="Email tidak valid")
    if len(phone) > 5:
        if not ValidPhoneNumber(phone):
            raise HTTPException(status_code=400, detail="NoHP tidak valid")

    dateOfBirth = convertStrDateToDate(dateOfBirth) if len(dateOfBirth) > 6 else None

    newPassword = PWDCONTEXT.encrypt(newPassword)
    update_op = await MGDB[tblName].update_one(
        {"userId": ObjectId(current_user.userId)},
        {
            "$set": {
                "email": email,
                "phone": phone,
                "isWa": isWa,
                "identity.dateOfBirth": dateOfBirth,
                "credential.password": newPassword,
                "credential.isFirstLogin": False,
            }
        },
    )

    find_op = await MGDB[tblName].find_one({"userId": ObjectId(current_user.userId)})
    name = find_op["name"] if find_op else None
    role = find_op["credential"]["role"] if find_op else None

    if update_op.modified_count:
        # penggantian password disimpan ke notif
        user_op = await MGDB[tblName].find_one(
            {"userId": ObjectId(current_user.userId)}
        )
        notif = NotifData()
        notif.title = "PENGGANTIAN PASSWORD"
        notif.name = name
        notif.role = role
        notif.email = user_op["email"]
        notif.message = (
            "Penggantian Password berhasil, Password anda saat ini : " + newPassword
        )
        notif.isSecret = True
        await CreateNotif(False, user_op["userId"], user_op["companyId"], notif)
        return {"status": "ok"}
    else:
        raise HTTPException(status_code=304)


@router_auth.get("/cek_token", response_model=dict)
async def cek_token(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    dComp = await GetCompanyOr404(current_user.companyId)

    # ini baru admin. user2 yang lain potensial bakal harus dibikin seperti ini juga
    user = await MGDB["user_admin"].find_one({"userId": ObjectId(current_user.userId)})
    if user is None:
        user = await MGDB["user_membership_member"].find_one(
            {"userId": ObjectId(current_user.userId)}
        )

    profileImage = ""
    if "profileImage" in user:
        profileImage = user["profileImage"]

    phone = ""
    if "phone" in user:
        phone = user["phone"]

    isWa = False
    if "isWa" in user:
        isWa = user["isWa"]

    dateOfBirth = ""
    if "dateOfBirth" in user:
        dateOfBirth = user["dateOfBirth"]

    isFirstLogin = False
    if "isFirstLogin" in user:
        isFirstLogin = user["isFirstLogin"]

    language = "id"
    if "language" in user:
        language = user["language"]

    name = ""
    if "name" in user:
        name = user["name"]

    email = ""
    if "email" in user:
        email = user["email"]

    if ENVIRONMENT == "development" or ENVIRONMENT == "staging":
        # return current_user
        return {
            "access_token": "",
            "userId": current_user.userId,
            "companyId": current_user.companyId,
            "companyName": dComp["name"],
            "active": True,
            "isAdmin": current_user.isAdmin,
            "role": current_user.roles,
            "name": name,
            "image": profileImage,
            "email": email,
            "phone": phone,
            "isWa": isWa,
            "dateOfBirth": dateOfBirth,
            "isFirstLogin": isFirstLogin,
            "lang": language,
        }
    else:
        return {"status": 200}


@router_auth.post("/user_change_lang", response_model=dict)
async def user_change_language(
    lang: LanguageEnum,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    tblName = current_user.tblName
    await MGDB[tblName].update_one(
        {"userId": ObjectId(current_user.userId)}, {"$set": {"lang": lang}}
    )
    return {"status": "ok", "lang": lang}


@router_auth.post("/user_set_automatic_login", response_model=dict)
async def user_set_automatic_login(
    autoLogin: AutomaticLoginEnum,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    tblName = current_user.tblName
    await MGDB[tblName].update_one(
        {"userId": ObjectId(current_user.userId)},
        {"$set": {"automaticLogin": autoLogin}},
    )
    return {"status": "ok", "automatic login": autoLogin}


@router_auth.post("/user_change_password", response_model=dict)
async def user_change_password(
    oldPassword: str,
    newPassword: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    tblName = current_user.tblName
    user_op = await MGDB[tblName].find_one({"userId": ObjectId(current_user.userId)})
    if not user_op:
        raise HTTPException(status_code=400, detail="Your Account is Disabled")

    if not PWDCONTEXT.verify(oldPassword, user_op["credential"]["password"]):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Incorrect old password"
        )

    newPass = PWDCONTEXT.encrypt(newPassword)
    update_op = await MGDB[tblName].update_one(
        {"userId": ObjectId(current_user.userId)},
        {"$set": {"credential.password": newPass, "credential.isFirstLogin": False}},
    )

    find_op = await MGDB[tblName].find_one({"userId": ObjectId(current_user.userId)})
    name = find_op["name"] if find_op else None
    role = find_op["credential"]["role"] if find_op else None

    if update_op.modified_count:
        # penggantian password disimpan ke notif
        notif = NotifData()
        notif.title = "PENGGANTIAN PASSWORD"
        notif.name = name
        notif.role = role
        notif.email = user_op["email"]
        notif.message = (
            "Penggantian Password berhasil, Password anda saat ini : " + newPassword
        )
        notif.isSecret = True
        await CreateNotif(False, user_op["userId"], user_op["companyId"], notif)
        return {"status": "ok"}
    else:
        raise HTTPException(status_code=304)


@router_auth.post("/user_forgot_password/{userTipe}", response_model=dict)
async def user_forgot_password(
    userTipe: UserTipeEnum, phone_or_email: str, backgroundTask: BackgroundTasks
):
    tblName = userTipe
    if ValidateEmail(phone_or_email):
        parameter = {"email": phone_or_email}
    else:
        if ValidPhoneNumber(phone_or_email):
            parameter = {"phone": phone_or_email}
        else:
            raise HTTPException(status_code=400, detail="Email atau NoHP tidak valid")

    user_op = await MGDB[tblName].find_one(parameter)

    if not user_op:
        raise HTTPException(status_code=400, detail="Email atau NoHP tidak terdaftar")

    otp = RandomNumber(4)
    tokenUrl = RandomString(20)
    waktuNow = dateTimeNow()

    if user_op["credential"]["lastResetPassword"]:
        waktuLastReset = user_op["credential"]["lastResetPassword"]
        selisih = (waktuNow - waktuLastReset).total_seconds()
        print(selisih)
        if selisih < 300:
            raise HTTPException(status_code=400, detail="Reset Password terlalu cepat")

    update_op = await MGDB[tblName].update_one(
        {"userId": user_op["userId"]},
        {
            "$set": {
                "credential.otp": otp,
                "credential.isFirstLogin": True,
                "credential.token": tokenUrl,
                "credential.lastResetPassword": waktuNow,
            }
        },
    )

    find_op = await MGDB[tblName].find_one(parameter)
    name = find_op["name"] if find_op else None
    role = find_op["credential"]["role"] if find_op else None

    notif = NotifData()
    notif.title = "RESET PASSWORD"
    notif.name = name
    notif.role = role
    notif.email = user_op["email"]
    notif.message = (
        "Hai " + user_op["name"] + ", Berikut OTP untuk reset password. OTP = " + otp
    )
    await CreateNotif(True, user_op["userId"], user_op["companyId"], notif)
    # kirim email

    emailBase = EmailData()
    emailBase.emailTo = phone_or_email
    emailBase.token = otp
    # backgroundTask.add_task(SendResetPasswordEmail, emailBase)

    if update_op.modified_count:
        if ENVIRONMENT == "development":
            return {"otp": otp, "tokenUrl": tokenUrl}
        else:
            return {"status": "ok"}
    else:
        raise HTTPException(status_code=304)


@router_auth.post("/user_cek_otp_after_forgot/{tipe}/{userTipe}", response_model=dict)
async def user_cek_otp_after_forgot(
    tipe: str, userTipe: UserTipeEnum, phone_or_email: str, key: str
):
    tblName = userTipe
    if len(key) < 4:
        raise HTTPException(status_code=400, detail="OTP or KEY is not valid")
    if ValidateEmail(phone_or_email):
        parameter = {"email": phone_or_email}
    else:
        if ValidPhoneNumber(phone_or_email):
            parameter = {"phone": phone_or_email}
        else:
            raise HTTPException(
                status_code=400, detail="Email or Phone Number is invalid"
            )
    user_op = await MGDB[tblName].find_one(parameter)
    if not user_op:
        raise HTTPException(status_code=400, detail="Phone Number is not registered")
    else:
        if user_op["credential"][tipe] == key:
            # otp benar, lanjut buat token utk create password
            newKey = RandomString(20)
            update_op = await MGDB[tblName].update_one(
                {"userId": user_op["userId"]},
                {
                    "$set": {
                        "credential.otp": "",
                        "credential.isFirstLogin": True,
                        "credential.token": newKey,
                    }
                },
            )
            if update_op.modified_count:
                return {"key": newKey}
            else:
                raise HTTPException(status_code=304)
        else:
            raise HTTPException(status_code=400, detail="KEY is not valid")


@router_auth.post("/user_create_password_after_forgot/{userTipe}", response_model=dict)
async def user_create_password_after_forgot(
    userTipe: UserTipeEnum, phone_or_email: str, key: str, newPassword: str
):
    tblName = userTipe
    ValidPassword(newPassword)
    if ValidateEmail(phone_or_email):
        parameter = {"email": phone_or_email}
    else:
        if ValidPhoneNumber(phone_or_email):
            parameter = {"phone": phone_or_email}
        else:
            raise HTTPException(
                status_code=400, detail="Email or Phone Number is invalid"
            )
    user_op = await MGDB[tblName].find_one(parameter)
    if not user_op:
        raise HTTPException(status_code=400, detail="Phone Number is not registered")
    else:
        if user_op["credential"]["token"] == key:
            # otp benar, lanjut buat token utk create password
            newPassword = PWDCONTEXT.encrypt(newPassword)
            update_op = await MGDB[tblName].update_one(
                {"userId": user_op["userId"]},
                {
                    "$set": {
                        "credential.password": newPassword,
                        "credential.isFirstLogin": False,
                        "credential.token": "",
                    }
                },
            )
            if update_op.modified_count:
                return {"status": "ok"}
            else:
                raise HTTPException(status_code=304)
        else:
            raise HTTPException(status_code=400, detail="KEY is not valid")


@router_auth.get("/reset_password_by_admin", response_model=UserBasic)
async def reset_password_by_admin(
    userTipe: UserTipeEnum,
    companyId: str,
    userId: str,
    current_user: JwtToken = Security(
        get_current_user, scopes=["admin,superadmin", "*"]
    ),
):
    tblName = userTipe
    if "superadmin" in current_user.roles:
        parameter = {"companyId": ObjectId(companyId), "userId": ObjectId("userId")}
    else:
        parameter = {
            "companyId": ObjectId(current_user.companyId),
            "userId": ObjectId("userId"),
        }

    user_op = await MGDB[tblName].find_one(parameter)

    if not user_op:
        raise HTTPException(status_code=404, detail="User tidak ditemukan")

    otp = RandomNumber(4)
    tokenUrl = RandomString(20)
    waktuNow = dateTimeNow()

    if user_op["credential"]["lastResetPassword"]:
        waktuLastReset = user_op["credential"]["lastResetPassword"]
        selisih = (waktuNow - waktuLastReset).total_seconds()
        print(selisih)
        if selisih < 300:
            raise HTTPException(status_code=400, detail="Reset Password terlalu cepat")

    update_op = await MGDB[tblName].update_one(
        {"userId": user_op["userId"]},
        {
            "$set": {
                "credential.otp": otp,
                "credential.isFirstLogin": True,
                "credential.token": tokenUrl,
                "credential.lastResetPassword": waktuNow,
            }
        },
    )

    find_op = await MGDB[tblName].find_one({"userId": ObjectId(current_user.userId)})
    name = find_op["name"] if find_op else None
    role = find_op["credential"]["role"] if find_op else None

    notif = NotifData()
    notif.title = "RESET PASSWORD"
    notif.name = name
    notif.role = role
    notif.email = user_op["email"]
    notif.message = (
        "Hai " + user_op["name"] + ", Berikut OTP untuk reset password. OTP = " + otp
    )
    await CreateNotif(True, user_op["userId"], user_op["companyId"], notif)
    # kirim email

    emailBase = EmailData()
    emailBase.emailTo = user_op["email"]
    emailBase.token = otp
    # backgroundTask.add_task(SendResetPasswordEmail, emailBase)

    if update_op.modified_count:
        if ENVIRONMENT == "development":
            return {"otp": otp, "tokenUrl": tokenUrl}
        else:
            return {"status": "ok"}
    else:
        raise HTTPException(status_code=304)


@router_auth.get("/logout", response_model=dict)
async def logout_for_clear_token():
    print("logout")
    return {"status": 200}


async def login_for_access_webview(form_data: OAuth2PasswordRequestForm = Depends()):
    # login dengan nohp atau username
    form_data.username = form_data.username.strip()
    arUsername = form_data.username.split("*")
    tblName = arUsername[0]
    username = arUsername[1]
    form_data.password = form_data.password.strip()
    companyId = ""
    # cari company id berdasarkan username atau ada di parameter login
    if username.count(".") == 1 and not username.count("@") == 1:
        arInitial = username.split(".")
        dCompany = await GetCompByInitial(arInitial[0])
        companyId = str(dCompany["companyId"])
    if len(arUsername) == 3:
        companyId = arUsername[2]
    data_token = await create_token_by_username_password(
        tblName, username, form_data.password, companyId
    )
    user = await MGDB[tblName].find_one({"userId": ObjectId(data_token.userId)})
    print(data_token.companyId)
    dComp = await GetCompanyOr404(data_token.companyId)
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    # if(data_token.exp = )
    if ENVIRONMENT == "development":
        access_token_expires = timedelta(minutes=30 * 24 * 60)
    access_token = create_access_token(data_token, expires_delta=access_token_expires)

    return {
        "access_token": access_token,
        "userId": data_token.userId,
        "companyId": data_token.companyId,
        "companyName": dComp["name"],
        "active": True,
        "isAdmin": data_token.isAdmin,
        "role": data_token.roles,
        "name": user["name"],
        "image": user["profileImage"],
        "email": user["email"],
        "phone": user["phone"],
        "isWa": user["isWa"],
        "dateOfBirth": user["identity"]["dateOfBirth"],
        "isFirstLogin": user["credential"]["isFirstLogin"],
    }
