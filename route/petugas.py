from util.util_excel import getNumber, getString
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security, File, UploadFile, Request, Form
from fastapi.security import (
    OAuth2PasswordBearer,
    OAuth2PasswordRequestForm,
    SecurityScopes,
)
from typing import List
from datetime import datetime
import logging
import math
import pandas as pd

from model.util import SearchRequest, FieldObjectIdRequest, DefaultData
from model.default import JwtToken, UserCredential
from util.util import ListToUp, ValidateObjectId, IsiDefault, CreateCriteria, PWDCONTEXT
from route.auth import get_current_user
from util.util_waktu import dateTimeNow
from model.petugas import petugasBase, petugasOnDB, petugasPage
from function.user import CreateUser

router_petugas = APIRouter()
dbase = MGDB.user_petugas

async def GetpetugasOr404(id_: str):
    _id = ValidateObjectId(id_)
    petugas = await dbase.find_one({"_id": ObjectId(_id)})
    # print(petugas)
    if petugas:
        return petugas
    else:
        raise HTTPException(status_code=404, detail="Data Not Found")

        # =================================================================================

@router_petugas.post("/petugas", response_model = dict())
async def add_petugas(
    data_in: petugasBase,
    request: Request = None,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
) :
    # token = await request.json()
    # current_user = await get_current_user(["*", "*"], token = token["token"]) 
    # print(data_in)

    petugas = UserCredential()
    petugas.nik = data_in.nik
    petugas.address = data_in.address
    petugas.username = data_in.nik
    petugas.name = data_in.name
    petugas.companyId = ObjectId(current_user.companyId)
    petugas.creatorUserId = ObjectId(current_user.userId)
    petugas.phone = "0000000000"
   

    # print(petugas)
    cpetugas = await dbase.find_one(
        {"nik": petugas.nik}
    )
    if cpetugas:
        raise HTTPException(
            status_code=400, detail="NIK sudah terdaftar"
        )

    user_op = await CreateUser(
        "user_petugas", petugas, data_in.password , False, ["petugas"]
    )           

    petugas_op = await dbase.find_one(
        {"nik": petugas.nik}
    )
    # print(petugas_op["_id"])
    # if petugas_op['_id']:
    #     petugass = await GetpetugasOr404(petugas_op['_id'])
    #     print(petugass)
    #     return {"data" : petugass}
    
    # print(petugas_op)
    return {"status": 200}

@router_petugas.post("/get_petugas", response_model = dict())
async def get_all_petugas(
    size: int = 1000,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = -1,
    search: SearchRequest = None,
    request : Request = None,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
    
) :
    skip = page * size
    search.defaultObjectId.append(
        FieldObjectIdRequest(field="creatorUserId", key=current_user.userId)
    )
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)

    reply = petugasPage(
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    # print(size)
    return reply

@router_petugas.put("/petugas/{id_}", response_model=petugasOnDB)
async def update_petugas(
    id_: str,
    data_in: petugasBase,
    request : Request,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    # token = await request.json()
    
    # current_user = await get_current_user(["*", "*"], token=token["token"])

    petugas = IsiDefault(data_in, current_user, True)
    petugas.updateTime = dateTimeNow()
    petugas.username = petugas.nik

    old = await dbase.find_one(
        {"isDelete": False, "_id":ObjectId(id_)}
    )

    # print(old["nik"] + "  " + petugas.nik)

    if old["nik"] != petugas.nik :
        # print ("tes")
        cpetugas = await dbase.find_one(
            {"creatorUserId": ObjectId(current_user.userId), "nik": petugas.nik, "isDelete": False}
        )
        if cpetugas:
            raise HTTPException(
                status_code=400, detail="NIK baru sudah terdaftar"
        )
    # print(petugas)
    petugas_op = await dbase.update_one(
        {"_id": ObjectId(id_), "creatorUserId": ObjectId(current_user.userId)},
        {"$set": petugas.dict(skip_defaults=True)},
    )
    if petugas_op.modified_count:
        return await GetpetugasOr404(id_)
    else:
        raise HTTPException(status_code=304, detail="Failed")


@router_petugas.delete("/petugas/{id_}", dependencies=[Depends(GetpetugasOr404)], response_model=dict)
async def delete_petugas_by_id(
    id_: str, 
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    # petugas_op = await dbase.delete_one(
    #     {"_id": ObjectId(id_), "creatorUserId": ObjectId(current_user.userId)}
    # )
    # if petugas_op.deleted_count:
    #     return {"status": f"deleted count: {petugas_op.deleted_count}"}
    # else:
    #     raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")

    petugas_op = await dbase.update_one(
        {"_id": ObjectId(id_), "creatorUserId": ObjectId(current_user.userId)},
        {"$set": {"isDelete" : True}},
    )

    if petugas_op.modified_count:
        return {"status": f"deleted count: {petugas_op.modified_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")

@router_petugas.post("/change_pass_admin", response_model=dict)
async def change_pass_petugas_by_admin(
    request : Request,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    datas = await request.json()
    password = PWDCONTEXT.encrypt(datas["password"])

    print(password)
    
    petugas_op = await dbase.update_one(
        {"_id": ObjectId(datas["id_"]), "creatorUserId": ObjectId(current_user.userId)},
        {"$set": {"credential.password": password,}},
    )

    if petugas_op.modified_count:
        return {"status": f"deleted count: {petugas_op.modified_count}"}
    else:
        raise HTTPException(status_code=404, detail="ID Not Found")


    # control for admin/home
@router_petugas.get("/count_home", response_model=dict)
async def count_home_admin (
    request : Request,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):

    petugas_cursor = dbase.find({"isDelete": False})
    petugas = await petugas_cursor.to_list(length=100000)
    count_petugas = len(petugas)

    laundry_cursor = MGDB.tbl_laundry.find({"isDelete": False})
    laundry = await laundry_cursor.to_list(length=100000)
    count_laundry = len(laundry)

    return {
        "count_petugas" : count_petugas,
        "count_laundry" : count_laundry
    }
    # print(count)

@router_petugas.post("/change_pass", response_model=dict)
async def petugas_change_pass (
    request : Request,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    datas = await request.json()

    petugas = await dbase.find_one({"userId": ObjectId(current_user.userId)})
    if not PWDCONTEXT.verify(datas["old_password"], petugas["credential"]["password"]) :
        return {"detail" : "Password lama tidak cocok"}

    password = PWDCONTEXT.encrypt(datas["new_password"])

    # print(petugas)
    
    petugas_op = await dbase.update_one(
        {"_id": petugas["_id"]},
        {"$set": {"credential.password": password,}},
    )

    if petugas_op.modified_count:
        return {"status": f"deleted count: {petugas_op.modified_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")