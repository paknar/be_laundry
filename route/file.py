from function.ppdb.pendaftar import (
    GetDocumentPPDBOr404,
    GetInvoicePPDBOr404,
    GetPendaftarOr404,
    UpdateInvoicePPDB,
)
from function.user import GetUserOr404
from function.document import GetDocumentOr404, GetDocumentSettingOr404, UpdateDocument
from model.support import (
    DocumentData,
    DocumentOnDB,
    DocumentSettingOnDB,
    InvoiceData,
    ParameterData,
)
from definitions import OPEN_FILE_ROOT, PROJECT_ROOT_PATH, TEMPLATE_DOC_PATH
from function.crm_katalis.client import GetClientOr404
from function.invoice import GetInvoiceOr404, GetUserOnInvoiceId, UpdateInvoice
from bson.objectid import ObjectId
from config.config import MGDB, MGIMAGE, CONF
from fastapi import (
    APIRouter,
    Depends,
    HTTPException,
    File,
    UploadFile,
    BackgroundTasks,
    Security,
)
from fastapi.responses import FileResponse
from typing import List
import shutil
import os
from PIL import Image
from autocrop import Cropper
import math
from os import path
import io
import base64

from route.auth import get_current_user
from model.default import JwtToken
from model.image import MediaBase, MediaPage, ActionType, ImageType, UploadBase64
from starlette.requests import Request
from util.util import (
    SearchRequest,
    RandomString,
    ValidateSizePage,
    CreateCriteria,
    FieldObjectIdRequest,
    IsiDefault,
)
from util.util_waktu import dateTimeNow, dateNowStr
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from fastapi import Form
from fastapi.responses import FileResponse
from fastapi.responses import RedirectResponse
from docxtpl import DocxTemplate

FOLDER = "./upload-file/"
IMAGE = "./file/"
dbase = MGIMAGE.tbl_file

router_file = APIRouter()

templates = Jinja2Templates(directory="template-html")

# =================================================================================


@router_file.post("/upload_file_aja", response_model=dict)
async def upload_file_aja(file: UploadFile = File(...)):
    upload = open(os.path.join(FOLDER, file.filename), "wb+")
    shutil.copyfileobj(file.file, upload)
    upload.close()
    return {"status": "ok"}


@router_file.get("/get_invoice_file/{parrentId}/{childrenId}/invoice.pdf")
async def get_invoice_file(parrentId: str, childrenId: str):
    return {"ok"}


@router_file.get("/get_invoice_data/{parrentId}/{childrenId}")
async def get_invoice_data(parrentId: str, childrenId: str):
    return {"ok"}


@router_file.get("/get_document/{documentId}", response_class=HTMLResponse)
async def get_document(documentId: str, request: Request):
    linkIsiParameter = OPEN_FILE_ROOT + "/document/isi_parameter/" + documentId
    document = await GetDocumentOr404(documentId)
    client = await GetClientOr404(str(document["userId"]))
    docSettingId = document["docSettingId"]
    docSetting = await GetDocumentSettingOr404(docSettingId)

    documentParameters = {}
    for param in document["parameter"]:
        key = param["key"]
        value = param["value"]
        documentParameters[key] = value

    docSettingParameters = docSetting["parameter"]

    return templates.TemplateResponse(
        "isi_parameter_document.html",
        {
            "request": request,
            "name": client["name"],
            "title": document["title"],
            "docSettingParameters": docSettingParameters,
            "documentParameters": documentParameters,
            "linkIsiParameter": linkIsiParameter,
        },
    )


@router_file.get(
    "/get_document_ppdb/{documentId}", response_class=RedirectResponse, status_code=302
)
async def get_document_ppdb(documentId: str):
    return OPEN_FILE_ROOT + "/generate_document_ppdb/" + documentId


@router_file.post("/document/isi_parameter/{documentId}", response_model=dict)
async def document_isi_parameter(documentId: str, data: List[ParameterData]):
    for i in range(len(data)):
        data[i] = data[i].dict()
    document = await UpdateDocument(documentId, {"parameter": data})
    return {"url": OPEN_FILE_ROOT + "/generate_document/" + documentId}


def deleteGeneratedDocument(file_location: str):
    os.remove(file_location)


@router_file.get("/generate_document_ppdb/{documentId}", response_class=FileResponse)
async def generate_document_ppdb(documentId: str, background_tasks: BackgroundTasks):
    document = await GetDocumentPPDBOr404(documentId)
    docSetting = await GetDocumentSettingOr404(str(document["docSettingId"]))
    filename = docSetting["templateFilename"]
    if filename is None:
        raise HTTPException(
            status_code=404, detail="File template document setting tidak ditemukan"
        )
    template = DocxTemplate(TEMPLATE_DOC_PATH + "/" + filename)
    parameter = {}
    for param in document["parameter"]:
        parameter[param["key"]] = param["value"]
    template.render(parameter)
    filename = document["title"] + ".docx"
    file_location = TEMPLATE_DOC_PATH + "/" + filename
    template.save(TEMPLATE_DOC_PATH + "/" + filename)
    background_tasks.add_task(deleteGeneratedDocument, file_location)
    return FileResponse(
        file_location,
        media_type="application/octet-stream",
        filename=filename,
    )


@router_file.get("/generate_document/{documentId}", response_class=FileResponse)
async def generate_document(documentId: str, background_tasks: BackgroundTasks):
    document = await GetDocumentOr404(documentId)
    docSetting = await GetDocumentSettingOr404(str(document["docSettingId"]))
    filename = docSetting["templateFilename"]
    template = DocxTemplate(TEMPLATE_DOC_PATH + "/" + filename)
    document = await GetDocumentOr404(documentId)
    parameter = {}
    for param in document["parameter"]:
        parameter[param["key"]] = param["value"]
    template.render(parameter)
    filename = document["title"] + ".docx"
    file_location = TEMPLATE_DOC_PATH + "/" + filename
    template.save(TEMPLATE_DOC_PATH + "/" + filename)
    background_tasks.add_task(deleteGeneratedDocument, file_location)
    return FileResponse(
        file_location,
        media_type="application/octet-stream",
        filename=filename,
    )


@router_file.get("/checkout_invoice/{invoiceId}", response_class=HTMLResponse)
async def checkout_invoice(invoiceId: str, request: Request):
    linkUpdateCaraBayar = OPEN_FILE_ROOT + "/invoice/update_cara_bayar/" + invoiceId
    invoice = await GetInvoiceOr404(invoiceId)
    user = await GetUserOr404(str(invoice["userId"]))
    parameters = invoice
    parameters["name"] = user["name"]
    parameters["linkUpdateCaraBayar"] = linkUpdateCaraBayar
    parameters["request"] = request
    if invoice["paymentMethod"]:
        del parameters["linkUpdateCaraBayar"]
        parameters["paymentMethod"] = invoice["paymentMethod"]
    return templates.TemplateResponse(
        "checkout_invoice.html",
        parameters,
    )


@router_file.get("/checkout_invoice_ppdb/{invoiceId}", response_class=HTMLResponse)
async def checkout_invoice_ppdb(invoiceId: str, request: Request):
    linkUpdateCaraBayar = (
        OPEN_FILE_ROOT + "/invoice_ppdb/update_cara_bayar/" + invoiceId
    )
    invoice = await GetInvoicePPDBOr404(invoiceId)
    user = await GetPendaftarOr404(str(invoice["userId"]))
    parameters = invoice
    parameters["name"] = user["name"]
    parameters["linkUpdateCaraBayar"] = linkUpdateCaraBayar
    parameters["request"] = request
    if invoice["paymentMethod"]:
        del parameters["linkUpdateCaraBayar"]
        parameters["paymentMethod"] = invoice["paymentMethod"]
    return templates.TemplateResponse(
        "checkout_invoice.html",
        parameters,
    )


@router_file.post(
    "/invoice/update_cara_bayar/{invoiceId}",
    response_class=RedirectResponse,
    status_code=302,
)
async def invoice_update_cara_bayar(invoiceId: str, caraBayar: str = Form(...)):
    data = {"paymentMethod": caraBayar}
    await UpdateInvoice(invoiceId, data)
    return OPEN_FILE_ROOT + "/checkout_invoice/" + invoiceId


@router_file.post(
    "/invoice_ppdb/update_cara_bayar/{invoiceId}",
    response_class=RedirectResponse,
    status_code=302,
)
async def invoice_ppdb_update_cara_bayar(invoiceId: str, caraBayar: str = Form(...)):
    data = {"paymentMethod": caraBayar}
    await UpdateInvoicePPDB(invoiceId, data)
    return OPEN_FILE_ROOT + "/checkout_invoice_ppdb/" + invoiceId


@router_file.post("/update_invoice_cara_bayar/{parrentId}/{childrenId}")
async def update_invoice_cara_bayar(parrentId: str, childrenId: str):
    return {"ok"}


# =================================================================================


@router_file.get("/get_document_file/{parrentId}/{childrenId}/invoice.pdf")
async def get_document_file(parrentId: str, childrenId: str):
    return {"ok"}


@router_file.get("/get_document_data/{parrentId}/{childrenId}")
async def get_document_data(parrentId: str, childrenId: str):
    return {"ok"}


@router_file.post("/update_document_data/{parrentId}/{childrenId}")
async def update_document_data(parrentId: str, childrenId: str):
    return {"ok"}


# =================================================================================


@router_file.get("/get_tiket_image/{customerId}/{ticketId}/{key}")
async def get_tiket_image(customerId: str, ticketId: str, key: str):
    return {"ok"}
