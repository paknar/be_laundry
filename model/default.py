from enum import Enum

# from model.membership.member import LanguageEnum
from model.support import InvoiceBase, InvoiceData, LanguageEnum

from pydantic.types import NonPositiveFloat
from util.util_waktu import dateNowStr
from util.util import RandomNumber, RandomString
from bson.objectid import ObjectId
from pydantic import BaseModel, Field
from typing import List
from datetime import date, datetime
from .util import ObjectIdStr, DefaultData, DefaultPage
from faker import Faker

fake = Faker()


class UserTipeEnum(str, Enum):
    user_admin = "user_admin"  # simpan di tabel admin
    user_membership_member = "user_membership_member"
    user_membership_kasir = "user_membership_kasir"
    user_membership_edc = "user_membership_edc"
    user_edmedia_pelajar = "user_edmedia_pelajar"
    user_edmedia_pengajar = "user_edmedia_pengajar"
    user_crmkatalis_partner = "user_crmkatalis_partner"
    user_crmkatalis_client = "user_crmkatalis_client"  # sekolah
    user_wisata_pengunjung = "user_wisata_pengunjung"
    user_wisata_gateway = "user_wisata_gateway"
    user_wisata_kasir = "user_wisata_kasir"
    user_wisata_miniatm = "user_wisata_miniatm"

    # ini: user_ppdb dan user_ppdb_customer
    # asumsi sekarang: ROLE_USER adalah admin, sedangkan ROLE_CUSTOMER adalah pendaftar
    user_ppdb = "ROLE_USER"
    user_ppdb_customer = "ROLE_CUSTOMER"
    user_ppdb_pendaftar = "user_ppdb_pendaftar"


class CoordinateData(BaseModel):
    type: str = "Point"
    coordinates: List[float] = []


class AddressData(BaseModel):
    province: str = None
    city: str = None
    district: str = None
    village: str = None
    street: str = None
    posCode: str = None
    location: CoordinateData = None
    note: str = None


class SolutionEnum(str, Enum):
    admin = "admin"
    crmkatalis = "crmkatalis"
    crmppdb = "crmppdb"
    membership = "membership"
    queue = "queue"
    edmedia = "edmedia"
    attendance = "attendance"
    transaction = "transaction"
    ppob = "ppob"
    wisata = "wisata"
    ticketing = "ticketing"
    accounting = "accounting"
    marketplace = "marketplace"
    parkir = "parkir"
    asset = "asset"


class MobileFeatureEnum(
    str, Enum
):  # menu mobile apps for user, not for device or admin
    menu_balance = "balance_info"  # info nama + saldo, info top up
    menu_transaction = "menu_transaction"  # payQR, invoice, mutasi, transaksi
    custom_donation = "custom_donation"
    custom_attendance = "custom_attendance"
    custom_door_access = "custom_door_access"
    ppob = "ppob"
    qris = "qris"
    transfer_bank = "transfer_bank"
    wisata = "wisata"
    queue = "queue"
    edmedia = "edmedia"
    parkir = "parkir"
    marketplace = "marketplace"


class ConfigVaData(BaseModel):
    bank: str = None
    biaya: str = None
    coaCode: str = None
    prefix: str = None
    clientId: str = None
    secretKey: str = None
    url: str = None
    bankAccount: str = None
    bankAccName: str = None


class CompanyInput(BaseModel):
    name: str = None
    initial: str = None
    address: AddressData = None
    tags: List[str] = []
    solution: List[SolutionEnum] = []
    mobileAppsFeature: List[MobileFeatureEnum] = []
    picName: str = None
    picEmail: str = None
    picPhone: str = None
    maxMember: int = 0

    class Config:
        schema_extra = {
            "example": {
                "name": "PT " + fake.name(),
                "initial": RandomString(2),
                "address": {"street": fake.address()},
                "tags": [],
                "solution": [
                    "admin",
                    "wisata",
                    "edmedia",
                    "crmkatalis",
                    "crmppdb",
                    "marketplace",
                ],
                "mobileAppsFeature": [],
                "picName": fake.name(),
                "picEmail": fake.first_name() + "@coba.com",
                "picPhone": "0813" + RandomNumber(6),
                "maxMember": 1000,
            }
        }


class CompanyBasic(DefaultData, CompanyInput):
    katalisCompanyCode: str = None
    katalisCompanyId: ObjectIdStr = None
    logoImage: str = None
    customKey: str = None
    waGatewayNumber: str = None
    configVa: List[ConfigVaData] = []


class CompanyOnDB(CompanyBasic):
    id: ObjectIdStr = Field(alias="_id")


class CompanyPage(DefaultPage):
    content: List[CompanyOnDB] = []


class GenderEnum(str, Enum):
    male = "male"
    female = "female"


class RoleEnum(str, Enum):
    superadmin = "superadmin"
    admin = "admin"
    kasir = "kasir"
    edc = "edc"
    customerService = "customerService"
    device = "device"
    kiosk = "kiosk"
    monitor = "monitor"
    merchant = "merchant"
    user = "user"
    sales = "sales"
    delivery = "delivery"
    stokist = "stokist"
    customer = "customer"
    pelajar = "pelajar"
    pengajar = "pengajar"
    partner = "partner"
    client = "client"
    member = "member"
    pendaftar = "pendaftar"


class CredentialData(BaseModel):
    tblName: str = None
    isActive: bool = True
    isAdmin: bool = False
    role: List[RoleEnum] = []
    firebase: str = None
    otp: str = None
    password: str = None
    loginCount: int = None
    deviceIdentity: List[str] = []
    isFirstLogin: bool = True
    wrongPasswordCount: int = None
    lastResetPassword: datetime = None
    resetPasswordCount: int = None
    token: int = None
    lastLogin: datetime = None


class UserMaritalStatus(str, Enum):
    divorced = "DIVORCED"
    married = "MARRIED"
    single = "SINGLE"
    windowed = "WIDOWED"
    other = "OTHER"


class UserReligion(str, Enum):
    islam = "ISLAM"
    kristen = "KRISTEN"
    katolik = "KATOLIK"
    hindu = "HINDU"
    budha = "BUDHA"
    konghucu = "KONGHUCU"
    other = "OTHER"


class IdentityData(BaseModel):
    dateOfBirth: date = None
    placeOfBirth: str = None
    gender: GenderEnum = None


class TipeNikEnum(str, Enum):
    ktp = "ktp"
    sim = "sim"
    passport = "passport"


class UserInput(BaseModel):
    tipeNik: TipeNikEnum = None  # ktp, sim, passport
    nik: str = None
    noId: str = None
    name: str = None
    email: str = None
    phone: str = None  #
    isWa: bool = False
    tags: List[str] = []
    address: AddressData = None  #
    identity: IdentityData = None

    class Config:
        schema_extra = {
            "example": {
                "tipeNik": "ktp",
                "nik": RandomNumber(6),
                "noId": RandomNumber(3),
                "name": fake.name(),
                "email": fake.first_name() + "@coba.com",
                "phone": "0813" + RandomNumber(6),
                "isWa": False,
                "tags": [],
                "address": {"street": fake.address(), "note": "note"},
                "identity": {
                    "dateOfBirth": dateNowStr(),
                    "placeOfBirth": "Jakarta",
                    "gender": "male",
                },
            }
        }


class UserBasic(DefaultData, UserInput):
    userId: ObjectIdStr = None
    username: str = None
    profileImage: str = None
    note: str = None


class UserPage(DefaultPage):
    content: List[UserBasic] = []


class UserCredential(UserBasic):
    credential: CredentialData = None


class UserOnDB(UserBasic):
    id: ObjectIdStr = Field(alias="_id")


class UserMinim(BaseModel):
    userId: ObjectIdStr = None
    name: str = None
    noId: str = None


class BalanceData(BaseModel):
    coaCode: str = None
    balance: int = 0
    lastUpdate: datetime = datetime.now()


class MutationData(BaseModel):
    id: ObjectIdStr = None
    coaCode: str = None
    createTime: datetime = datetime.now()
    name: str = None
    amount: int = 0
    balance: int = 0


class VaNumberData(BaseModel):
    id: ObjectIdStr = None
    bankName: str = None
    vaNumber: str = None
    vaName: str = None
    isOpenVa: bool = True
    isTKI: bool = False
    amount: int = 0
    expired: datetime = None


class NomorRekeningBank(BaseModel):
    id: ObjectIdStr = None
    namaBank: str = None
    nomorRekening: str = None
    atasNama: str = None


class UserDataBase(BaseModel):
    createTime: datetime = datetime.now()
    updateTime: datetime = datetime.now()
    companyId: ObjectIdStr = None
    userId: ObjectIdStr = None
    noId: str = None
    pin: str = None
    nfcId: str = None
    nfcStatus: bool = True
    qrCard: str = None
    qrExpired: datetime = None
    qrClosedCode: str = None
    qrClosedExpired: datetime = None
    qrClosedAmount: datetime = None
    images: List[str] = []
    norekBank: List[NomorRekeningBank] = []
    balances: List[BalanceData] = []
    mutation: List[MutationData] = []
    vaNumber: List[VaNumberData] = []
    invoicesData: List[InvoiceData] = []
    name: str = None
    email: str = None

    # Tambah proses update special merchant
    # i.e. di specialMerchant ini bakal ada value kayak "Rumah sakit a", "rumah sakit b", dll.
    # di user dan di merchant sama2 di cek apakah value specialMerchant-nya sama apa nggak.
    # ini untuk rujukan.
    # merchant / pihak rumah sakit, bisa update value specialMerchant dari si user

    specialMerchant: List[str] = []


class UserDataOnDB(UserDataBase):
    id: ObjectIdStr = Field(alias="_id")


class UserCard(BaseModel):
    companyId: ObjectIdStr = None
    userId: ObjectIdStr = None
    name: str = None
    noId: str = None
    qrCard: str = None
    qrExpired: datetime = None
    nfcId: str = None
    images: List[str] = []
    address: AddressData = None
    identity: IdentityData = None


class AksesBase(BaseModel):
    createTime: datetime = None
    waktuAkses: datetime = None
    suhuTubuh: float = 0
    namaDevice: str = None
    tipe: str = None


class JenisAbsen(str, Enum):
    none = "none"
    izin = "izin"
    datang_tepat = "datang_tepat"
    datang_terlambat = "datang_terlambat"
    pulang_awal = "pulang_awal"
    pulang_tepat = "pulang_tepat"
    lembur = "lembur"


class TanggalAbsen(BaseModel):
    tanggal: date = None
    first: str = None
    suhuFirst: float = 0
    last: str = None
    suhuLast: float = 0
    masuk: JenisAbsen = "none"
    pulang: JenisAbsen = "none"
    keterangan: str = None
    namaShift: str = None


class CutiBase(BaseModel):
    createTime: datetime = None
    updateTime: datetime = None
    tglCuti: date = None
    isPermitted: bool = False
    keterangan: str = None


class UserAksesBase(BaseModel):
    createTime: datetime = datetime.now()
    updateTime: datetime = datetime.now()
    companyId: ObjectIdStr = None
    userId: ObjectIdStr = None
    noId: str = None
    lastDevice: str = None
    lastTipe: str = None
    lastTime: datetime = None
    akses: List[AksesBase] = []
    absen: List[TanggalAbsen] = []
    cuti: List[CutiBase] = []


class UserAksesOnDB(UserAksesBase):
    id: ObjectIdStr = Field(alias="_id")


class OauthTipe(str, Enum):
    google = "google"
    facebook = "facebook"


class RegisterInput(BaseModel):
    name: str = None
    oauthType: OauthTipe = None  # google / facebook
    oauthImage: str = None
    email: str = None
    phone: str = None
    password: str = None
    tblName: UserTipeEnum = None


class RegisterBase(DefaultData, RegisterInput):
    captcha: str = None
    ipSource: str = None
    otpCode: str = None
    otpStatus: bool = False
    expiredTime: datetime = None


class RegisterOnDB(RegisterBase):
    id: ObjectIdStr = Field(alias="_id")


class NotifData(BaseModel):
    userId: ObjectIdStr = None
    id: ObjectIdStr = None
    createTime: datetime = None
    title: str = None
    name: str = None
    role: list = []
    email: str = None
    phone: str = None
    message: str = None
    readTime: datetime = None
    isSecret: bool = False
    isDelete: bool = False


class NotifBase(BaseModel):
    createTime: datetime = datetime.now()
    updateTime: datetime = datetime.now()
    companyId: ObjectIdStr = None
    userId: ObjectIdStr = None
    notif: List[NotifData] = []


class NotifOnDB(NotifBase):
    id: ObjectIdStr = Field(alias="_id")


class NotifMembershipData(BaseModel):
    # pass
    userId: ObjectIdStr = None
    id: ObjectIdStr = None
    createTime: datetime = datetime.now()
    title: str = None
    message: str = None
    readTime: datetime = None


class NotifMembershipBase(BaseModel):
    createTime: datetime = datetime.now()
    updateTime: datetime = datetime.now()
    userId: ObjectIdStr = None
    notif: List[NotifMembershipData] = []


class ArrayNotifs(BaseModel):
    notif: NotifData = None


class NotifPage(DefaultPage):
    # content: List[ArrayNotifs] = []
    content: List[NotifData] = []


class JwtToken(BaseModel):
    tblName: UserTipeEnum = None
    producer: str = None
    userId: ObjectIdStr = None
    companyId: ObjectIdStr = None
    roles: List[str] = []
    isAdmin: bool = False
    exp: int = 1000
    lang: LanguageEnum = "id"


class Token(BaseModel):
    access_token: str
    token_type: str


class JwtBase(JwtToken):
    id: str = None
    jwtKey: str = None
    expiredTime: datetime = None
    ip: str = None


class InquiryBase(BaseModel):
    createTime: datetime = datetime.now()
    expiredTime: datetime = datetime.now()
    code: str = None
    companyId: ObjectIdStr = None
    userId: ObjectIdStr = None
    userDestination: ObjectIdStr = None
    billingNumber: str = None
    isUsed: bool = False
    trxId: str = None
    reff: str = None
    amount: int = None
    note: str = None
    data: dict = {}


class InquiryOnDB(InquiryBase):
    id: ObjectIdStr = Field(alias="_id")
