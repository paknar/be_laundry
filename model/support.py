from util.util_waktu import dateTimeNow
from bson.objectid import ObjectId
from enum import Enum
from pydantic import BaseModel, Field
from typing import List
from datetime import datetime, date

from pydantic.utils import Obj
from .util import DefaultPage, ObjectIdStr, DefaultData
import random


class InvoiceSetPriceData(BaseModel):
    setting: str = None
    price: int = 0


class InvoiceSettingBase(BaseModel):
    invoiceSettingName: str = None
    serviceName: str = None
    defaultPrice: int = 0
    settingPrice: List[InvoiceSetPriceData] = None


class InvoiceSettingOnDB(InvoiceSettingBase):
    id: ObjectIdStr = Field(alias="_id")


class PaymentStatusEnum(str, Enum):
    unpaid = "unpaid"
    paid = "paid"
    partial = "partial"


class InvoiceDetail(BaseModel):
    id: ObjectIdStr = None
    itemName: str = None
    price: int = None
    qty: int = None
    subTotal: int = None


class HistoryPayment(BaseModel):
    id: ObjectIdStr = None
    createTime: datetime = None
    userId: ObjectIdStr = None
    payment: str = None
    note: str = None
    source: str = None


# class InvoiceAction(BaseModel):
#     action: str = None


class InvoiceInput(BaseModel):
    tags: List[str] = []
    invoiceDate: datetime = dateTimeNow()
    invoiceYear: int = None
    number: str = None
    title: str = None
    template: str = None
    item: List[InvoiceDetail] = []
    paymentStatus: PaymentStatusEnum = "unpaid"
    total: int = None
    paid: int = None
    discount: int = None
    tax: int = None
    final: int = None
    expiredDate: datetime = None  #
    historyPayment: List[HistoryPayment] = []
    userId: ObjectIdStr = None
    action: str = None
    paymentMethod: str = None
    paymentNumberId: str = (
        None  # bisa diisi nomor va, nomor pembayaran, code pembayaran dll
    )
    paymentAdminCost: str = None
    paymentTotal: str = None
    paymentReff: str = None

    class Config:
        schema_extra = {
            "example": {
                "tags": [],
                "invoiceDate": datetime.now(),
                "invoiceYear": None,
                "number": None,
                "title": "Random Invoice",
                "template": None,
                "item": [],
                "paymentStatus": "unpaid",
                "total": 100000,
                "paid": None,
                "discount": None,
                "tax": None,
                "final": None,
                "historyPayment": [],
                "userId": "613eba9d6e0ac818055e5356",
                "action": "http://url.com/blablabla",
            }
        }


class InvoiceData(InvoiceInput, DefaultData):
    id: ObjectIdStr = None


class InvoicePage(DefaultPage):
    content: List[InvoiceData] = []


class InvoiceBase(DefaultData):
    title: str = None
    tags: List[str] = []
    month: int = Field(gt=0, lt=13)
    year: int
    invoices: List[InvoiceData] = []
    action: str = None
    invoiceBaseId: ObjectIdStr = None


class InvoiceOnDB(InvoiceBase):
    id: ObjectIdStr = Field(alias="_id")


class ParameterData(BaseModel):
    key: str = None
    tipe: str = None
    choice: List[str] = []
    isMultipleChoice: bool = False
    value: str = None

    class Config:
        schema_extra = {
            "example": {
                "key": "Hello",
                "tipe": "string",
                "choice": [],
                "isMultipleChoice": False,
                "value": "World",
            }
        }


class DocumentSettingBase(DefaultData):
    documentSettingName: str = None
    serviceName: str = None
    parameter: List[str] = []
    templateFilename: str = None

    class Config:
        schema_extra = {
            "example": {
                "documentSettingName": "Dokumen Penawaran Katalis",
                "serviceName": "Katalis",
            }
        }


class DocumentSettingPage(DefaultData):
    content: List[DocumentSettingBase] = []


class DocumentSettingOnDB(DocumentSettingBase):
    id: ObjectIdStr = Field(alias="_id")


class DocumentInput(BaseModel):
    number: str = None
    date: datetime = dateTimeNow()
    title: str = None
    kepada: str = None
    template: str = None
    parameter: List[ParameterData] = []
    tags: List[str] = []
    isOpen: bool = False
    userId: ObjectIdStr = None
    docSettingId: ObjectIdStr = None

    class Config:
        schema_extra = {
            "example": {
                "tags": [],
                "date": datetime.now(),
                "number": None,
                "title": "Dokumen A",
                "template": None,
                "kepada": "John Doe",
                "parameter": [],
                "userId": "613eba9d6e0ac818055e5356",
                "docSettingId": "613ad92ab9124f9b94cd444c",
            }
        }


class DocumentData(DocumentInput, DefaultData):
    id: ObjectIdStr = None
    linkDocument: str = None


class DocumentPage(DefaultPage):
    content: List[DocumentData] = []


class DocumentBase(DefaultData):
    title: str = None
    tags: List[str] = []
    month: int = Field(gt=0, lt=13)
    # asumsi sekarang, year berdasarkan InvoiceDate
    year: int
    documents: List[DocumentData] = []


class DocumentOnDB(DocumentBase):
    id: ObjectIdStr = Field(alias="_id")


class InfoInput(BaseModel):
    title: str = None
    subTitle: str = None
    tags: List[str] = []
    description: str = None
    urlFb: str = None
    urlIg: str = None
    urlYoutube: str = None
    urlDownload: str = None

    class Config:
        schema_extra = {
            "example": {
                "title": "Halo Dunia",
                "subTitle": "Info Pertamaku",
                "tags": [],
                "description": "lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla dignissim fermentum ornare. Duis viverra libero ut elit vestibulum, a scelerisque lorem posuere. Sed sed elit vitae mi accumsan sagittis. Nam sollicitudin sapien vel nulla semper ultricies id vestibulum dolor. Etiam congue rutrum leo, quis maximus leo pellentesque non. Aenean a imperdiet ante, eget vulputate nulla. Etiam blandit ac tortor vel posuere. Pellentesque quis enim mauris. Nam sit amet feugiat felis. Integer sed sem in arcu facilisis fringilla eget at justo.",
                "urlFb": "",
                "urlIg": "",
            }
        }


class InfoBase(InfoInput, DefaultData):
    isPublish: bool = None
    isHeadLine: bool = None
    viewCount: int = None
    image: str = None


class InfoOnDB(InfoBase):
    id: ObjectIdStr = Field(alias="_id")


class InfoPage(DefaultPage):
    content: List[InfoOnDB] = []


class MessageData(BaseModel):
    createTime: str = None
    tokenId: str = None
    name: str = None
    userId: ObjectIdStr = None
    isRead: bool = None
    readTime: datetime = None
    message: str = None
    isDelete: bool = None


class ChatProfileData(BaseModel):
    name: str = None
    userId: ObjectIdStr = None
    tokenId: str = None
    phone: str = None


class ChatBase(BaseModel):
    createTime: datetime = None
    lastChatTime: datetime = None
    csProfile: ChatProfileData = None
    customerProfile: ChatProfileData = None
    note: str = None
    message: List[MessageData] = []


class ChatOnDB(ChatBase):
    id: ObjectIdStr = Field(alias="_id")


class DonationBase(DefaultData):
    title: str = None
    description: str = None
    image: str = None
    maximalWaktu: datetime = None
    danaDibutuhkan: int = None
    danaTerkumpul: int = None
    laporanPenyaluran: List[str] = []
    tags: List[str] = []


class DonationOnDB(DonationBase):
    id: ObjectIdStr = Field(alias="_id")


class PipelineData(BaseModel):
    id: ObjectIdStr = None
    createTime: datetime = None
    nomor: int = None
    # implementasi PSP atau pembiayaan atau buka sambungan baru atau keluhan
    name: str = None
    form: List[ParameterData] = []
    isPassed: bool = False
    isValidated: bool = False
    note: str = None

    class Config:
        schema_extra = {
            "example": {
                "id": "6131e479f830108ed629249a",
                "createTime": datetime.now(),
                "nomor": random.randint(1, 13),
                "name": "Surat Penawaran",
                "form": [],
                "isPassed": False,
                "isValidated": False,
                "note": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla dignissim fermentum ornare. Duis viverra libero ut elit vestibulum, a scelerisque lorem posuere. Sed sed elit vitae mi accumsan sagittis. Nam sollicitudin sapien vel nulla semper ultricies id ",
            }
        }


class AutomaticLoginEnum(str, Enum):
    active = "active"
    not_active = "not_active"


class LanguageEnum(str, Enum):
    id = "id"
    en = "en"


class ProvinceBase(BaseModel):
    id: str = None
    name: str = None


class RegencyBase(BaseModel):
    id: str = None
    province_id: str = None
    name: str = None


class DistrictBase(BaseModel):
    id: str = None
    regency_id: str = None
    name: str = None
