from datetime import date, datetime
from util.util_waktu import dateTimeNow
from pydantic import BaseModel, Field
from typing import List
from model.util import DefaultData, ObjectIdStr, DefaultPage
from model.default import AddressData, IdentityData, CredentialData, UserInput
from util.util import RandomNumber, RandomString
from faker import Faker


class petugasBase(DefaultData):
    nik: str = None
    name: str = None
    address: str = None
    password : str = None
    sandi : str = None
    userId : ObjectIdStr = None
    username : str = None



class petugasOnDB(petugasBase):
    id: ObjectIdStr = Field(alias="_id")


class petugasPage(DefaultPage):
    content: List[petugasOnDB] = []
