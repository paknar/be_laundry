from datetime import date, datetime
from util.util_waktu import dateTimeNow
from pydantic import BaseModel, Field
from typing import List
from model.util import DefaultData, ObjectIdStr, DefaultPage
from model.default import AddressData, IdentityData, CredentialData, UserInput
from util.util import RandomNumber, RandomString
from faker import Faker
from model.petugas import petugasOnDB

class laundryBase(DefaultData) :
    nis : str = None
    name : str = None
    potong : str = None
    berat : str = None
    input_time : str = None
    status : str = None

class laundryOnDB(laundryBase):
    id: ObjectIdStr = Field(alias="_id")


class laundryPage(DefaultPage):
    content: List[laundryOnDB] = []
    data : List[petugasOnDB] = []