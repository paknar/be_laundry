from typing import List
from function.company import GetCompByInitial, GetCompanyOr404
from function.notif import CreateNotif
from model.util import EmailData
from model.default import (
    JwtToken,
    NotifBase,
    NotifData,
    RoleEnum,
    UserAksesBase,
    UserCredential,
    CredentialData,
    AddressData,
    UserDataBase,
    UserTipeEnum,
    VaNumberData,
)
from bson.objectid import ObjectId
from config.config import MGDB, CONF
from fastapi import HTTPException

from util.util import (
    CekNfcLen,
    RandomString,
    ValidPassword,
    ValidPhoneNumber,
    ValidateEmail,
    ValidateObjectId,
    RandomNumber,
    PWDCONTEXT,
    CheckList,
    ConvertLevelToFlat,
    FlattenArray,
    cleanNullTerms,
)
from util.util_waktu import convertStrDateToDate, dateTimeNow
from datetime import timedelta
import requests


async def GetUserOr404(id: str):
    _id = ValidateObjectId(id)
    dbase = MGDB.user_data
    user = await dbase.find_one({"userId": _id})
    if user:
        return user
    else:
        raise HTTPException(status_code=404, detail="User tidak ditemukan")


async def GetUserByNfcOr404(nfcId: str):
    nfc = CekNfcLen(nfcId)
    dbase = MGDB.user_data
    user = await dbase.find_one({"nfcId": nfc})
    if user:
        print("ada kartu")
        return user
    else:
        raise HTTPException(status_code=404, detail="Kartu tidak terdaftar")


def CreateCredentialTemporary(
    tblName: UserTipeEnum, companyId: str, email: str, temporary: str
):
    print(email + " - " + temporary)
    dbase = MGDB[tblName]
    dbase.update_one(
        {"email": email, "companyId": ObjectId(companyId)},
        {"$set": {"credential.otp": temporary}},
    )
    return True


async def CheckEmailWithCompany(tblName: UserTipeEnum, email: str, companyId: str):
    dbase = MGDB[tblName]
    data = await dbase.find_one({"email": email, "companyId": ObjectId(companyId)})
    if data:
        return True
    else:
        return False


async def CheckEmailOnly(tblName: UserTipeEnum, email: str):
    dbase = MGDB[tblName]
    data = await dbase.find({"email": email})
    if data:
        return data
    else:
        return False


async def CheckMemberByUserId(userId: str, companyId: str):
    dbase = MGDB["user_membership_member"]
    data = await dbase.find_one(
        {"userId": ObjectId(userId), "companyId": ObjectId(companyId)}
    )
    if data:
        print("User ID ditemukan")
        return data
    else:
        edc = await CheckEdcByUserId(userId, companyId)
        if edc:
            return edc
        raise HTTPException(status_code=404, detail="User ID tidak ditemukan")


async def CheckEdcByUserId(userId: str, companyId: str):
    dbase = MGDB["user_membership_edc"]
    data = await dbase.find_one(
        {"userId": ObjectId(userId), "companyId": ObjectId(companyId)}
    )
    if data:
        print("User ID ditemukan")
        return data
    else:
        raise HTTPException(status_code=404, detail="User ID tidak ditemukan")


async def GetUserIdByNomorVA(noVA: str, bankName: str, companyId: str):
    dbase = MGDB["user_membership_member"]
    data = await dbase.find_one(
        {
            "companyId": ObjectId(companyId),
            "vaNumber.vaNumber": noVA,
            "vaNumber.bankName": bankName,
        }
    )
    if data:
        print("User ID ditemukan")
        return data
    else:
        raise HTTPException(status_code=404, detail="User ID tidak ditemukan")


async def GetAdminCompany(id: str):
    companyId = ValidateObjectId(id)
    company = await MGDB.user_admin.find_one(
        {"companyId": companyId, "note": "default"}
    )
    if company:
        return company
    else:
        raise HTTPException(status_code=404, detail="Admin Company not found")


# ================================================================


async def CreateUser(
    tblName: str,
    user: UserCredential,
    password: str,
    isFirstLogin: bool,
    role: List[RoleEnum],
):
    dbase = MGDB[tblName]
    dCompany = await GetCompanyOr404(str(user.companyId))
    # cek apakah company sudah memiliki solusi sesuai dengan tblName
    arTblName = tblName.split("_")
    # print(dCompany)
    if arTblName[1] not in dCompany["solution"]:
        raise HTTPException(
            status_code=400,
            detail="Company yang dipilih belum memiliki solusi " +
            arTblName[1],
        )

    user.username = user.username.lower()
    user.createTime = dateTimeNow()
    user.updateTime = dateTimeNow()
    user.name = user.name.upper()
    user.userId = ObjectId()
    # print(user)
    if ValidateEmail(user.email):
        user.email = user.email.lower()
        user_email = await dbase.find_one(
            {
                "email": user.email,
                "companyId": ObjectId(user.companyId),
                "isDelete": False,
            }
        )
        if user_email:
            raise HTTPException(
                status_code=400,
                detail="Email sudah terdaftar, gunakan email yang lain atau silahkan login",
            )
    else:
        user.email = None

    if ValidPhoneNumber(user.phone):
        user_phone = await dbase.find_one(
            {
                "phone": user.phone,
                "companyId": ObjectId(user.companyId),
                "isDelete": False,
            }
        )
        # if user_phone:
        #     raise HTTPException(
        #         status_code=400,
        #         detail="NoHP sudah terdaftar, gunakan nohp yang lain atau silahkan login",
        #     )
    else:
        user.phone = None
        raise HTTPException(
            status_code=400,
            detail="Format nomor telepon salah. Panjang karakter minimal 8, dan maksimal 13 (antara 8-13 karakter)",
        )

    # user.identity.dateOfBirth = (
    #     convertStrDateToDate(user.identity.dateOfBirth)
    #     if user.identity.dateOfBirth
    #     else None
    # )

    credential = CredentialData()
    credential.tblName = tblName

    credential.password = PWDCONTEXT.encrypt(password)
    credential.wrongPasswordCount = 0
    credential.loginCount = 0
    credential.role = role
    credential.isFirstLogin = isFirstLogin
    user.credential = credential

    if (
        "superadmin" in role
        or "admin" in role
        or "kasir" in role
        or "customerService" in role
    ):
        user.credential.isAdmin = True
    else:
        user.credential.isAdmin = False

    await dbase.insert_one(user.dict())

    # insert to userdatabase,
    # uData = UserDataBase()
    # uData.companyId = user.companyId
    # uData.userId = user.userId
    # uData.noId = user.noId
    # uData.email = user.email
    # uData.name = user.name
    # await MGDB["user_data"].insert_one(uData.dict())

    # insert to userAksesdata
    # uAkses = UserAksesBase()
    # uAkses.companyId
    # uAkses.companyId = user.companyId
    # uAkses.userId = user.userId
    # uAkses.noId = user.noId
    # await MGDB["user_akses"].insert_one(uAkses.dict())

    # if ValidateEmail(user.email):
    #     print("kirim email")
    #     email = EmailData()
    #     email.projectName = dCompany["name"]
    #     email.name = user.name
    #     email.emailTo = user.email
    #     email.token = password
        # SendNewAccountEmail(EmailData)

    # notif = NotifData()
    # notif.title = "AKTIVASI AKUN"
    # notif.name = user.name.upper()
    # notif.role = role
    # notif.email = user.email
    # notif.message = (
    #     "Selamat bergabung di "
    #     + dCompany["name"]
    #     + ", Username : "
    #     + user.username
    #     + " atau Email atau Nohp, Password : "
    #     + password
    # )
    # notif.isSecret = True
    # await CreateNotif(False, user.userId, user.companyId, notif)

    return user


async def DeleteUser(tblName: str, idUser: str):
    dbase = MGDB[tblName]
    cek_dulu = await dbase.find_one({"userId": ObjectId(idUser)})
    if len(cek_dulu["akun"]) == 0:
        await dbase.delete_one({"userId": ObjectId(idUser)})
    return {"status": "ok"}


def checkIfUserAuthorized(current_user: JwtToken, userId: str):
    # if "admin" not in current_user.roles and "superadmin" not in current_user.roles:
    if "admin" not in current_user.roles and "superadmin" not in current_user.roles:
        if current_user.userId != userId:
            raise HTTPException(
                status_code=403, detail="User dilarang mengakses data user lain"
            )


# def checkIfUserAuthorized(current_user: JwtToken, companyId: str):
#     # if "admin" not in current_user.roles and "superadmin" not in current_user.roles:
#     if "superadmin" not in current_user.roles:
#         if current_user.userId != companyId:
#             raise HTTPException(
#                 status_code=403, detail="User dilarang mengakses data user lain"
#             )


async def checkIfNoIdIsAlreadyRegistered(
    dbase, companyId: str, noId: str, tipeUser: RoleEnum
):
    user = await dbase.find_one({"companyId": ObjectId(companyId), "noId": noId})
    print("check if no id is already registered")
    print("user: " + str(user))
    if user:
        raise HTTPException(
            status_code=400,
            detail=tipeUser.capitalize() + " with NIP : " + noId + " is registered",
        )


async def CreateOpenVa(companyId: str, bankName: str, userId: str, vaName: str):
    # cek duplicate
    cek = await MGDB.user_data.find_one(
        {
            "companyId": ObjectId(companyId),
            "userId": ObjectId(userId),
            "vaNumber.bankName": bankName,
        }
    )
    if cek:
        raise HTTPException(
            status_code=400,
            detail="Nomor VA dengan Bank " + bankName + " sudah dimiliki oleh user",
        )

    # request ke gateway roso
    noVA = RandomNumber()
    data = VaNumberData()
    data.id = ObjectId()
    data.bankName = bankName
    data.vaNumber = noVA
    data.vaName = vaName
    data.isOpenVa = True
    data.isTKI = False
    await MGDB.user_data.update_one(
        {"companyId": ObjectId(companyId), "userId": ObjectId(userId)},
        {"$addToSet": {"vaNumber": data.dict()}},
    )
    # return {"ok"}
    return data


async def CreateOpenTkiVA(companyId: str, bankName: str, userId: str, vaName: str):
    # cek duplicate
    cek = await MGDB.user_data.update_one(
        {
            "companyId": ObjectId(companyId),
            "userId": ObjectId(userId),
            "vaNumber.bankName": bankName,
        }
    )
    if cek:
        raise HTTPException(
            status_code=400,
            detail="Nomor VA sudah dimiliki oleh user",
        )
    # TODO kirim request ke gateway dengan callback top up by system
    # request ke gateway roso
    noVA = RandomNumber()
    data = VaNumberData()
    data.id = ObjectId
    data.bankName = bankName
    data.vaNumber = noVA
    data.vaName = vaName
    data.isOpenVa = True
    data.isTKI = True
    await MGDB.user_data.update_one(
        {"companyId": ObjectId(companyId), "userId": ObjectId(userId)},
        {"$addToSet": {"vaNumber": data}},
    )
    return {"ok"}


def UpdateDateOfBirth(updateData: dict):
    if "identity" in updateData:
        updateData["identity"] = cleanNullTerms(updateData["identity"])
        if ("dateOfBirth") in updateData["identity"]:
            updateData["identity"]["dateOfBirth"] = convertStrDateToDate(
                updateData["identity"]["dateOfBirth"]
            )
    return updateData
