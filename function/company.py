from bson.objectid import ObjectId
from config.config import MGDB, CONF
from model.default import CompanyBasic
from util.util_waktu import dateTimeNow
from util.util import ValidateObjectId
from fastapi import HTTPException

dbase = MGDB.tbl_company


async def GetCompanyOr404(id: str):
    companyId = ValidateObjectId(id)
    company = await dbase.find_one({"companyId": companyId})

    if "isDelete" in company == False:
        company["isDelete"] = False

    if company and (company["isDelete"] == False):
        return company
    else:
        raise HTTPException(status_code=404, detail="Company not found")


async def GetCompByInitial(initial: str):
    company = await dbase.find_one({"initial": initial})
    if company:
        return company
    else:
        raise HTTPException(status_code=404, detail="Company not found")


async def CekDuplicate(name: str, initial: str):
    company = await dbase.find_one({"$or": [{"name": name}, {"initial": initial}]})
    print(company)
    if not company:
        return True
    else:
        raise HTTPException(
            status_code=400, detail="Duplicate Value, Company Name or Initials is exist"
        )


async def CreateCompany(comp: CompanyBasic):
    comp.createTime = dateTimeNow()
    comp.updateTime = dateTimeNow()
    comp.companyId = ObjectId()
    comp.name = comp.name.upper()
    comp.initial = comp.initial.upper()
    await MGDB.tbl_company.insert_one(comp.dict())
    return comp


async def UpdateCompany(companyId: str, data: dict):
    companyId = ValidateObjectId(companyId)
    dbase = MGDB.tbl_company
    await dbase.update_one(
        {"companyId": companyId},
        {"$set": data},
    )
    return await GetCompanyOr404(companyId)
