import pytz
from datetime import datetime
from fastapi import HTTPException

TIMEZONE = pytz.timezone('Asia/Jakarta')

def dateNow():
    timenow = datetime.now(tz = TIMEZONE)
    waktuSekarangStr = timenow.strftime("%Y-%m-%d")
    waktuSekarang = datetime.strptime(waktuSekarangStr, "%Y-%m-%d")
    return waktuSekarang

def dateTimeNow():
    timenow = datetime.now(tz = TIMEZONE)
    waktuSekarangStr = timenow.strftime("%Y-%m-%d %H:%M:%S")
    waktuSekarang = datetime.strptime(waktuSekarangStr, "%Y-%m-%d %H:%M:%S")
    # fmt = '%Y-%m-%d %H:%M:%S'
    return waktuSekarang #timenow.strftime(fmt)

def timeNow():
    timenow = datetime.now(tz = TIMEZONE)
    waktuSekarangStr = timenow.strftime("%H:%M:%S")
    waktuSekarang = datetime.strptime(waktuSekarangStr, '%H:%M:%S').time()
    return waktuSekarang

def dateTimeNowStr():
    timenow = datetime.now(tz = TIMEZONE)
    waktuSekarangStr = timenow.strftime("%Y-%m-%d %H:%M:%S")
    return waktuSekarangStr

def dateNowStr():
    timenow = datetime.now(tz = TIMEZONE)
    waktuSekarangStr = timenow.strftime("%Y-%m-%d")
    return waktuSekarangStr

def timeNowStr():
    timenow = datetime.now(tz = TIMEZONE)
    waktuSekarangStr = timenow.strftime("%H:%M:%S")
    return waktuSekarangStr

def convertStrDateToDate(strDate : str): 
    if strDate:       
        if type(strDate) == datetime:   
            return strDate
        elif type(strDate) == datetime.date:   
            return strDate
        else:
            date = datetime.strptime(str(strDate), '%Y-%m-%d')
            return date
    else:
        return None

def convertStrDateTimeToDateTime(strDate : str):
    try:     
        date = datetime.strptime(strDate, '%Y-%m-%d %H:%M:%S')
        return date
    except:
        raise HTTPException(status_code=400, detail="Format tanggal waktu tidak sesuai, contoh format = 2020-12-01 08:00:00")

def convertStrTimeToTime(strTime : str):
    try:
        time = datetime.strptime(strTime, '%H:%M').time()
        return time
    except:
        raise HTTPException(status_code=400, detail="Format waktu tidak sesuai, contoh format = 08:00")

def dateTimeToDateTimeIndo(dateTime : datetime): 
    fmt = '%Y-%m-%d %H:%M:%S'
    loc_dt = TIMEZONE.localize(dateTime)
    return loc_dt.strftime(fmt)

def convertDateToStrDate(date):  
    if date:  
        if type(date) == str:   
            return date
        else:
            date = datetime.strftime(date, '%Y-%m-%d')
            return date
    else:
        return None

def convertDateToStrPassword(date):  
    if date:  
        if type(date) == str:   
            return date
        else:
            date = datetime.strftime(date, '%d%m%y')
            return date
    else:
        return None

def bulanToNumber(bulan: str):
    awal = bulan[0:3].lower()
    if awal == 'jan':
        return '01'
    elif awal == 'feb':
        return '02'
    elif awal == 'mar':
        return '03'
    elif awal == 'apr':
        return '04'
    elif awal == 'mei' or awal == 'may':
        return '05'
    elif awal == 'jun':
        return '06'
    elif awal == 'jul':
        return '07'
    elif awal == 'agu' or awal == 'aug':
        return '08'
    elif awal == 'sep':
        return '09'
    elif awal == 'okt' or awal == 'oct':
        return '10'
    elif awal == 'nov':
        return '11'
    elif awal == 'des' or awal == 'dec':
        return '12'
