from bson.objectid import ObjectId
from config.config import DEBUG
from fastapi import HTTPException
from datetime import datetime
import re
import logging
import string
import random
import math
from passlib.context import CryptContext

from model.util import (
    SearchRequest,
    FieldRequest,
    FieldBoolRequest,
    FieldObjectIdRequest,
)
from util.util_waktu import dateTimeNow

PWDCONTEXT = CryptContext(schemes=["bcrypt"], deprecated="auto")

regexEmail = "^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,5}$"


def RandomNumber(stringLength=6):
    regexNumber = "0123456789"
    return "".join(random.choice(regexNumber) for i in range(stringLength))


def RandomString(stringLength=6):
    letters = string.ascii_lowercase
    return "".join(random.choice(letters) for i in range(stringLength))


def ValidateObjectId(id: str):
    try:
        _id = ObjectId(id)
    except Exception:
        if DEBUG == True:
            logging.warning("Invalid Object ID")
        raise HTTPException(status_code=400)
    return _id


def ValidateObjectAkunId(idAkun: str):
    try:
        _id = ObjectId(idAkun)
    except Exception:
        if DEBUG == True:
            logging.warning("Invalid Object ID")
        raise HTTPException(status_code=400)
    return _id


def CekValidObjectId(id_: str):
    try:
        ObjectId(id_)
        return True
    except Exception:
        return False


def ValidPhoneNumber(phone_number: str):
    if not phone_number:
        return False
    if 7 < len(phone_number) and len(phone_number) < 14 and phone_number.isnumeric():
        return True
    else:
        return False


def ValidateEmail(email: str):
    if not email:
        return False
    if email.count("@") > 0:
        return True
    else:
        return False


def ValidPassword(pwd: str):
    if 5 < len(pwd) < 21:
        return True
    else:
        raise HTTPException(status_code=400, detail="Password length between 6 - 20")


def CheckList(list1, list2):
    for x in list1:
        if x in list2:
            return True
    return False


def ConvertLevelToFlat(f):
    out = {}
    data = []

    def flatten(x, name=None):
        if type(x) is dict:
            for a in x:
                data.append(a)
                val = ".".join((name, a)) if name else a
                flatten(x[a], val)
        elif type(x) is list:
            for (i, a) in enumerate(x):
                if type(a) is str:
                    data.append(a)
                flatten(a, name + f"[{str(i)}]")
        else:
            out[name] = x if x else ""

    flatten(f)
    return data


def FlattenJson(nested_json):
    out = {}

    def flatten(x, name=""):
        if type(x) is dict:
            for a in x:
                flatten(x[a], name + a + "_")
        elif type(x) is list:
            i = 0
            for a in x:
                flatten(a, name + str(i) + "_")
                i += 1
        else:
            out[name[:-1]] = x

    flatten(nested_json)
    return out


def FlattenArray(nested_json, key: str):

    okey = []

    def flatten(x, name=""):
        if type(x) is dict:
            for a in x:
                flatten(x[a], name + a + "*")
                print(a + "=" + key)
                if a == key:
                    okey.append(name + a)
        elif type(x) is list:
            i = 0
            for a in x:
                flatten(a, name)
                i += 1
        else:

            print(x + "=" + key)
            if x == key:
                okey.append(name[:-1] + "*" + x)

    flatten(nested_json)
    return okey


def ConvertToMongodb(obj):
    result = {}
    for key, val in obj.items():
        if not isinstance(val, dict):
            result[key] = val
            continue

        for sub_key, sub_val in val.items():
            new_key = "{}.{}".format(key, sub_key)
            result[new_key] = sub_val
            if not isinstance(sub_val, dict):
                continue

            result.update(ConvertToMongodb(result))
            if new_key in result:
                del result[new_key]

    return result


def ConvertToMgdb(kunci, obj):
    result = {}
    for key, val in obj.items():
        result[kunci + ".$." + key] = val
    return result


def ConvertToMgdebe(kunci, obj):
    result = {}
    for key, val in obj.items():
        result[kunci + ".$[i]." + key] = val
    return result


def DollarListDetail(obj):
    for key in list(obj.keys()):
        new_key = "listDetail.$." + key
        obj[new_key] = obj.pop(key)
        new_key = ""
    return obj


def DollarVaNumber(obj):
    for key in list(obj.keys()):
        new_key = "vaNumbers.$." + key
        obj[new_key] = obj.pop(key)
        new_key = ""
    return obj


def IsiDefault(pengguna, current_user, isUpdate: bool = False):
    if isUpdate:
        del pengguna.createTime
    else:
        pengguna.createTime = dateTimeNow()
    pengguna.updateTime = dateTimeNow()
    pengguna.companyId = ObjectId(current_user.companyId)
    pengguna.creatorUserId = ObjectId(current_user.userId)
    pengguna.isDelete = False
    return pengguna


def CreateCriteria(search: SearchRequest):
    criteria = []

    criteria.append({"isDelete": False})

    for ii in search.search:
        if len(ii.field) > 2:
            criteria.append({ii.field: {"$regex": ii.key, "$options": "i"}})

    for kk in search.fixSearch:
        if len(kk.field) > 2:
            criteria.append({kk.field: kk.key})

    for kk in search.numbSearch:
        if len(kk.field) > 2:
            criteria.append({kk.field: kk.key})

    for jj in search.tagSearch:
        if len(jj.field) > 2:
            criteria.append({jj.field: {"$all": jj.key}})

    for jj in search.tagInSearch:
        if len(jj.field) > 2:
            criteria.append({jj.field: {"$in": jj.key}})

    for kk in search.defaultBool:
        if len(kk.field) > 2:
            criteria.append({kk.field: kk.key})

    for ll in search.defaultObjectId:
        if len(ll.field) > 2:
            criteria.append({ll.field: ObjectId(ll.key)})

    if len(criteria) > 0:
        criteria = {"$and": criteria} if len(criteria) > 0 else {}
    else:
        criteria = {}

    return criteria


def UserCreateCriteria(search: SearchRequest, isAdmin: bool):
    criteria = []

    criteria.append({"isDelete": False})

    for ii in search.search:
        if len(ii.field) > 2:
            criteria.append({ii.field: {"$regex": ii.key, "$options": "i"}})

    for jj in search.tagSearch:
        if len(jj.field) > 2:
            criteria.append({jj.field: {"$all": jj.key}})

    for jj in search.tagInSearch:
        if len(jj.field) > 2:
            criteria.append({jj.field: {"$in": jj.key}})

    for kk in search.defaultBool:
        if len(kk.field) > 2:
            criteria.append({kk.field: kk.key})

    for ll in search.defaultObjectId:
        if len(ll.field) > 2:
            criteria.append({ll.field: ObjectId(ll.key)})

    criteria.append({"akun": {"$elemMatch": {"isAdmin": isAdmin}}})

    if len(criteria) > 0:
        criteria = {"$and": criteria} if len(criteria) > 0 else {}
    else:
        criteria = {}

    return criteria


def DeviceCreateCriteria(search: SearchRequest, isAdmin: bool):
    criteria = []

    criteria.append({"isDelete": False})

    for ii in search.search:
        if len(ii.field) > 2:
            criteria.append({ii.field: {"$regex": ii.key, "$options": "i"}})

    for jj in search.tagSearch:
        if len(jj.field) > 2:
            criteria.append({jj.field: {"$all": jj.key}})

    for jj in search.tagInSearch:
        if len(jj.field) > 2:
            criteria.append({jj.field: {"$in": jj.key}})

    for kk in search.defaultBool:
        if len(kk.field) > 2:
            criteria.append({kk.field: kk.key})

    for ll in search.defaultObjectId:
        if len(ll.field) > 2:
            criteria.append({ll.field: ObjectId(ll.key)})

    criteria.append(
        {"akun": {"$elemMatch": {"isAdmin": isAdmin, "role": {"$in": ["ROLE_DEVICE"]}}}}
    )

    if len(criteria) > 0:
        criteria = {"$and": criteria} if len(criteria) > 0 else {}
    else:
        criteria = {}

    return criteria


def CreateArrayCriteria(namaArray: str, search: SearchRequest):
    criteria = []

    for ii in search.search:
        if len(ii.field) > 2:
            cari = namaArray + "." + ii.field
            criteria.append({cari: {"$regex": ii.key, "$options": "i"}})

    for kk in search.fixSearch:
        if len(kk.field) > 2:
            cari = namaArray + "." + kk.field
            criteria.append({cari: kk.key})

    for kk in search.numbSearch:
        if len(kk.field) > 2:
            cari = namaArray + "." + kk.field
            criteria.append({cari: kk.key})

    for jj in search.tagSearch:
        if len(jj.field) > 2:
            cari = namaArray + "." + jj.field
            criteria.append({cari: {"$all": jj.key}})

    for jj in search.tagInSearch:
        if len(jj.field) > 2:
            cari = namaArray + "." + jj.field
            criteria.append({cari: {"$in": jj.key}})

    for kk in search.defaultBool:
        if len(kk.field) > 2:
            cari = namaArray + "." + kk.field
            criteria.append({kk.field: kk.key})

    for ll in search.defaultObjectId:
        if len(ll.field) > 2:
            cari = namaArray + "." + ll.field
            criteria.append({cari: ObjectId(ll.key)})

    if len(criteria) > 0:
        criteria = {"$and": criteria} if len(criteria) > 0 else {}
    else:
        criteria = {}

    return criteria


def ValidateSizePage(size: int = 10):
    if size > 200:
        raise HTTPException(status_code=400, detail="Maximal size is 200")
    else:
        return size


def mergeObjectProperties(objectToMergeFrom, objectToMergeTo):
    for property in objectToMergeFrom.__dict__:
        if property != "id":
            setattr(objectToMergeTo, property, getattr(objectToMergeFrom, property))

    return objectToMergeTo


def CekNfcLen(nfcid: str):
    nfcid = nfcid.upper()
    panjang = len(nfcid)
    if panjang == 8:
        nfcid = "00" + nfcid
    if panjang == 9:
        nfcid = "0" + nfcid
    return nfcid


def is_slice_in_list(s, l):
    len_s = len(s)  # so we don't recompute length of s on every iteration
    return any(s == l[i : len_s + i] for i in range(len(l) - len_s + 1))


def ListToUp(myList):
    if myList:
        for i in range(0, len(myList)):
            myList[i] = myList[i].upper().strip()
        return myList
    else:
        return []


def cleanNullTerms(data):
    # return {k: v for k, v in data.items() if (v is not None and v != [] )}
    new_data = {}
    for k, v in data.items():
        if isinstance(v, dict):
            v = cleanNullTerms(v)
        if not v in (u'', None, {}):
            new_data[k] = v
    return new_data
